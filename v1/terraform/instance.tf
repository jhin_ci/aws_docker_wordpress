provider "aws" {
  profile    = "default"
  region     = var.region
}

resource "aws_instance" "ec2-test" {
  ami           = var.amis[var.region]
  instance_type = "t2.micro"

  provisioner "local-exec" {
    command = "echo ${aws_instance.ec2-test.public_ip} > ip_address.txt"
  }
}
resource "aws_eip" "ip" {
  instance = aws_instance.ec2-test.id
}
