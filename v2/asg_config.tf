#Lunch template for the wordpress_asg autoscaling group
resource "aws_launch_configuration" "wordpress_launch" {
  name_prefix          = "web_config"
  image_id      = "ami-07b4156579ea1d7ba"
  instance_type = "t2.micro"
#Associate a public ip address with an instance in a VPC
  associate_public_ip_address = true
  key_name = "wordpress-test"
  security_groups=["${aws_security_group.web.id}"]
#The config to provide when launching the instance
  user_data = "${data.template_file.serverconfig.rendered}"
}

#AutoScaling Group resource
resource "aws_autoscaling_group" "wordpress_asg" {
  name                 = "wordpress-asg"
  launch_configuration = "${aws_launch_configuration.wordpress_launch.name}"
  min_size             = 2
  max_size             = 2
  vpc_zone_identifier = ["${aws_subnet.public-sub-a.id}","${aws_subnet.public-sub-b.id}"]
  load_balancers = ["${aws_elb.wordpress.id}"]
  lifecycle {
    #create a new instance and then destroy an old
    create_before_destroy = true
  }
}

#Template for server configuration
data "template_file" "serverconfig" {
  template = "${file("serverconfig.tpl")}"
  vars = {
    dbhost = "${aws_db_instance.wordpress-db.address}"
    efsid = "${aws_efs_file_system.wordpress-fs.id}"
  }
}
