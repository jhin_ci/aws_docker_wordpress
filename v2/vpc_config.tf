# VPC
resource "aws_vpc" "aws-wordpress" {
    cidr_block = "10.0.0.0/16"
    enable_dns_hostnames = true
    tags = {
    Name = "aws-wordpress"
  }
}

## A Public subnet
resource "aws_subnet" "public-sub-a" {
    vpc_id                  = "${aws_vpc.aws-wordpress.id}"
    cidr_block              = "10.0.1.0/24"
# Indicate that instances launched into the subnet should be assigned a public IP address
    map_public_ip_on_launch = "true"
    availability_zone = "${data.aws_availability_zones.available.names[0]}"
    tags = {
    Name = "public-sub-a"
  }

}
# B public subnet
resource "aws_subnet" "public-sub-b" {
    vpc_id                  = "${aws_vpc.aws-wordpress.id}"
    cidr_block              = "10.0.2.0/24"
# Indicate that instances launched into the subnet should be assigned a public IP address
    map_public_ip_on_launch = "true"
    availability_zone = "${data.aws_availability_zones.available.names[1]}"
    tags = {
    Name = "public-sub-b"
  }

}

# A private subnet
resource "aws_subnet" "private-sub-a" {
    vpc_id                  = "${aws_vpc.aws-wordpress.id}"
    cidr_block              = "10.0.3.0/24"
    availability_zone = "${data.aws_availability_zones.available.names[0]}"
    tags = {
    Name = "private-sub-a"
  }
}
# B private subnet
resource "aws_subnet" "private-sub-b" {
    vpc_id                  = "${aws_vpc.aws-wordpress.id}"
    cidr_block              = "10.0.4.0/24"
    availability_zone = "${data.aws_availability_zones.available.names[1]}"
    tags = {
    Name = "private-sub-b"
  }
}


# GW
resource "aws_internet_gateway" "gw" {
    vpc_id = "${aws_vpc.aws-wordpress.id}"
    tags = {
    Name = "aws-wordpress.internet_gateway"
  }

}

# Elastic IP
resource "aws_eip" "nateip" {
	 vpc      = true
}

#VPC NAT Gateway
resource "aws_nat_gateway" "natgw" {
# Allocation ID of the Elastic IP address for the gateway.
  allocation_id = "${aws_eip.nateip.id}"
# The Subnet ID of the subnet in which to place the gateway.
  subnet_id     = "${aws_subnet.public-sub-a.id}"
}

# Provides a resource to create a VPC routing table.
resource "aws_route_table" "priv_nat_route_table" {
    vpc_id = "${aws_vpc.aws-wordpress.id}"
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_nat_gateway.natgw.id}"
    }
}

resource "aws_route_table" "pub_igw_route_table" {
    vpc_id = "${aws_vpc.aws-wordpress.id}"
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.gw.id}"
    }
}

# Route table associations. Public route table
resource "aws_route_table_association" "public-sub-a" {
    subnet_id = "${aws_subnet.public-sub-a.id}"
    route_table_id = "${aws_route_table.pub_igw_route_table.id}"
}

resource "aws_route_table_association" "public-sub-b" {
    subnet_id = "${aws_subnet.public-sub-b.id}"
    route_table_id = "${aws_route_table.pub_igw_route_table.id}"
}


# Route table associations. Private route tables
resource "aws_route_table_association" "private-sub-a" {
    subnet_id = "${aws_subnet.private-sub-a.id}"
    route_table_id = "${aws_route_table.priv_nat_route_table.id}"
}

resource "aws_route_table_association" "private-sub-b" {
    subnet_id = "${aws_subnet.private-sub-b.id}"
    route_table_id = "${aws_route_table.priv_nat_route_table.id}"
}
