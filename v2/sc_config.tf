#Security group for WEB
resource "aws_security_group" "web" {
    name = "web-traffic"
    description = "Allow inbound traffic"
    vpc_id = "${aws_vpc.aws-wordpress.id}"

    tags = {
      Name = "web-traffic"
    }
# Open ports for group WEB
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

#Security group for DB
resource "aws_security_group" "db" {
    name = "db-traffic"
    description = "Allow inbound traffic"
    vpc_id = "${aws_vpc.aws-wordpress.id}"

    tags = {
      Name = "db-traffic"
    }

# Open ports for group DB
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        security_groups  = ["${aws_security_group.web.id}"]
    }

    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        security_groups  = ["${aws_security_group.web.id}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

# Security group for Elastic FS
resource "aws_security_group" "efs" {
    name = "efs-traffic"
    description = "Allow inbound traffic"
    vpc_id = "${aws_vpc.aws-wordpress.id}"

    tags = {
      Name = "db-traffic"
    }

# Open ports for group efs
    ingress {
        from_port = 2049
        to_port = 2049
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}
