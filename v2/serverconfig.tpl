#!/bin/bash -xe
whoami
apt-get -y update
apt-get -y install unattended-upgrades
apt-get -y install nfs-common
mkdir /efs
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${efsid}.efs.us-east-1.amazonaws.com:/ /efs
curl 'https://bootstrap.pypa.io/get-pip.py' -o 'get-pip.py'
sudo python get-pip.py
pip install awscli
curl -L get.docker.com | sudo bash
apt-get -y install fail2ban
pip install docker-compose
docker run -d -e WORDPRESS_DB_HOST=${dbhost}:3306 -e WORDPRESS_DB_PASSWORD=wordpress2019 -e WORDPRESS_DB_USER=dbadmin -e WORDPRESS_DB_NAME=wordpress  -v /efs/wordpress:/var/www/html -p 80:80 wordpress:latest
